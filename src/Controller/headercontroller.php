<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;




class headercontroller extends AbstractController
{
    #[Route('/header', name: 'app_header')]
  
    public function header(): Response
    {
        return $this->render('header.html.twig');
    }}