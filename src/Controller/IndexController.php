<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\CarteRepository;
use App\Repository\InfosRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class IndexController extends AbstractController
{
    #[Route('/', name: 'index_index',methods:['GET'])]
    public function index(CarteRepository $CarteRepository,InfosRepository $InfosRepository )
    {
        $contex= array('titre' => 'coucou' , 
        'cartes'=> $CarteRepository->findAll(), 
          'infos'=> $InfosRepository->findAll(), 
        
       
        'showEdit'=> false
    );
    

        return $this->render('accueil/home.html.twig',$contex);
    }

    
    

}


?>
