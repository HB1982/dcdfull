<?php

namespace App\Controller;

use App\Entity\Carte;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CarteRepository;
use App\Form\CarteType;



class MenuController extends AbstractController
{
    #[Route('/menu', name: 'app_menu')]
  
    public function index(CarteRepository $CarteRepository): Response
    {
        return $this->render('carte/listeproduits.html.twig', [
            'cartes' => $CarteRepository->findAll(),
        ]);
    }}