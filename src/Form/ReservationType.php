<?php

namespace App\Form;

use App\Entity\Reservation;
use App\Service\mailer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom',TextType::class,['attr' => ['class' => 'form-control mb-2']])
            ->add('telephone',TextType::class,['attr' => ['class' => 'form-control mb-2']])
            ->add('date', DateTimeType::class, [
                'hours' => [11,12,13,19,20,21,22],
                'minutes' => [0,15,30,45],
            ])
            ->add('nombre',NumberType::class,['attr' => ['class' => 'form-select']])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
